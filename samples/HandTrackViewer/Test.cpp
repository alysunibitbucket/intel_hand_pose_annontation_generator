#include "hsklu.h" // Tracking library API + 3D depth sensor support
#include <gl/freeglut.h> // For rendering output (OpenGL/FreeGLUT)
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <direct.h>
#include <string>
#include <stdint.h>
#include <boost/algorithm/string/join.hpp>

hskl::Tracker	hg_tracking;								// Tracking library context
float			hg_handWidth  = 0.08f;					// Width of hand, assume 8 cm to start
float			hg_handLength = 0.19f;					// Length of hand, assume 19 cm to start
const char *	hg_profileString = "left hand";			// Descripti: unexpected end of file while looking for precompiled header. Did you forget to add '#include "StdAfx.h"' to your sourceon of which hand is being tracked

int				hg_winWidth, hg_winHeight;				// Width and height of the rendering window
const float		hg_nearZ = .01f, hg_farZ = 2.f;			// Near and far clip distances for rendering
int				hg_time0, hg_time1, hg_numFrames, hg_fps;	// Timing control variables
bool			hg_layers[5] = {false,true,false,true,true};	// Rendering layers

bool			hg_recording = false;
int				hg_counter = 0;


cv::Mat get_visualizable_depth_img(cv::Mat& depth_img) {
	using namespace cv;
	double min;
	double max;

	minMaxIdx(depth_img, &min, &max);
	min = DBL_MAX;
	for (int x = 0; x < depth_img.cols; x++) {
		for (int y = 0; y < depth_img.rows; y++) {
			if (depth_img.at<int16_t>(y, x) != 0 && depth_img.at<int16_t>(y, x) < min) {
				min = depth_img.at<int16_t>(y, x);
			}
		}
	}

	cv::Mat m = depth_img.clone();
	m -= min;

	cv::Mat adjMap;
	cv::convertScaleAbs(m, adjMap, 255.0 / (max - min));
	//std::cout << " min is " << min << " max is " << max << std::endl;
	return adjMap;
}

#include <boost\filesystem.hpp>
#include <dirent.h>

const float labels[17][3] = { 
	{1, 1, 1}, // Forearm
	{1, 0, 0}, // Palm
	{0, .5, 0}, // Thumb1
	{0, 1, 0}, // Thumb2
	{.5, 1, .5}, // Thumb3
	{.5, 0, .5}, // Index1
	{1, 0, 1}, // Index2
	{1, .5, 1}, // Index3
	{.5, .5, 0}, // Middle1
	{1, 1, 0}, // Middle2
	{1, 1, .5}, // Middle3
	{0, .5, .5}, // Ring1
	{0, 1, 1}, // Ring2
	{.5, 1, 1}, // Ring3
	{0, 0, .5}, // Pinky1
	{0, 0, 1}, // Pinky2
	{.5, .5, 1} // Pinky3
};

const float a[16] = {252.686450, 1006.670652, 1001.850891, 1052.659947, 1009.336381, 997.363478, 994.278576, 1011.075164, 1001.215584, 998.850565, 1003.479420, 1002.230528, 1013.421658, 1000.341925, 1011.938343, 1012.794318};
const float b[16] = { -20.2262,  -24.7399,   -7.0172,  -23.5472,  -14.8356,  -13.1651,   -9.0130,  -15.0631, -14.0838,  -11.9694,  -12.2970,  -12.9018,  -15.9086,   -8.4984,  -11.3813,  -12.2925};


hskl::float4 hglToScreen(const hskl::float4 v) {

	// Get the matrices and viewport
	double modelView[] = {1,0,0,0,0,-1,0,0,0,0,-1,0,0,0,0,1};
	double projection[] = {1.40314,0,0,0,0,1.92078,0,0,0,0,-1.01005,-1,0,0,-0.0201005,0};
	double viewport[] = {0,0,640,480};
	double depthRange[] = {0,1};

	//glGetDoublev(GL_MODELVIEW_MATRIX, modelView);
	//glGetDoublev(GL_PROJECTION_MATRIX, projection);
	//glGetDoublev(GL_VIEWPORT, viewport);
	//glGetDoublev(GL_DEPTH_RANGE, depthRange);


	// Compose the matrices into a single row-major transformation
	float T[4][4];
	//hskl::float4x4 T;
	int r, c, i;
	for (r = 0; r < 4; ++r) {
		for (c = 0; c < 4; ++c) {
			T[r][c] = 0;
			for (i = 0; i < 4; ++i) {
				// OpenGL matrices are column major
				T[r][c] += projection[r + i * 4] * modelView[i + c * 4];
			}
		}
	}

	hskl::float4 T1[4];
	for (r=0; r<4; ++r) {
		T1[r] = hskl::float4(T[r][0], T[r][1], T[r][2],T[r][3]);
	}

	// Transform the vertex
	hskl::float4 result;
	result.x = hskl::dot(T1[0], v);
	result.y = hskl::dot(T1[1], v);
	result.z = hskl::dot(T1[2], v);
	result.w = hskl::dot(T1[3], v);

	//if(result.w==0) std::cout << T1[3][0] << "," << T1[3][1] << "," << T1[3][2] << "," << T1[3][3] << " " << v[0] << " " << v[1] << " " << v[2] << " " << v[3]  << std::endl;

	// Homogeneous divide
	const double rhw = 1 / result.w;


	double depth = (result.z * rhw) * (depthRange[1] - depthRange[0]) + depthRange[0];


	if(depth < 1.0){
		depth = -1 * ((hg_farZ * hg_nearZ)/(depth*(hg_farZ - hg_nearZ) - hg_farZ));

		//depth = 2*z_far*z_near/(z_far + z_near - (depth*2 - 1)*(z_far - z_near) );

	}
	else
	{
		depth = pow(2.,15)-1;
	}


	return hskl::float4(
		(1 + result.x * rhw) * viewport[2] / 2 + viewport[0],
		(1 - result.y * rhw) * viewport[3] / 2 + viewport[1],
		v.z, // Coefficients calculated from linear regression
		//depth,
		//(result.z * rhw) * (depthRange[1] - depthRange[0]) + depthRange[0]
		//(result.z * rhw) * (g_farZ - g_nearZ) + g_nearZ,
		//(result.z * rhw - g_nearZ) * (depthRange[1] - depthRange[0]) / (g_farZ - g_nearZ) + depthRange[0],
		//(result.z * rhw),
		rhw);

} 


hskl::float4 to_screen_coords(hskl::float4& v){
	//x_screen = (x_world/z_world)*f_x + c_x
	//y_screen = (y_world/z_world)*f_y + c_y
	// values from sensor are 
	float c_y = 320.0/2.0;
	float c_x = 240.0/2.0;
	float f_x = 1.23838;
	float f_y = 0.960016;


	float x = (v[0]/v[2])*f_x + c_x;
	float y = (v[1]/v[2])*f_y + c_y;

	hskl::float4 v2(x, y, v[2], 1);

	return v2;
}


cv::Mat display_joints(hskl::Tracker& g_tracking, float** joints, cv::Mat& display){
	cv::Mat new_disp = display.clone();
	// Joints
	int joint_mapping[] = {0, -1, -1, 14,15,-1, 11,12,-1, 8,9,-1, 5,6,-1, 2,3,-1};
	int tip_mapping[] = {16, 13, 10, 7, 4};

	for(int i=0; i<g_tracking.GetBoneCount();  i++)
	{
		int color_ind = joint_mapping[i];

		if(color_ind==-1) continue;

		//cv::circle(new_disp, cv::Point(joints[color_ind][0], joints[color_ind][1]), 6, cv::Scalar(labels[color_ind][0] * 255, labels[color_ind][1] * 255 , labels[color_ind][2] * 255), 2);
		cv::circle(new_disp, cv::Point(joints[color_ind][0], joints[color_ind][1]), 6, cv::Scalar(labels[color_ind][2] * 255, labels[color_ind][1] * 255 , labels[color_ind][0] * 255), 2);
	}

	for(int finger=0; finger<5; ++finger)  // to fingertips
	{
		int color_ind = tip_mapping[finger];

		if(color_ind==-1) continue;

		//	cv::circle(new_disp, cv::Point(joints[color_ind][0], joints[color_ind][1]), 6, cv::Scalar(labels[color_ind][0] * 255, labels[color_ind][1] * 255 , labels[color_ind][2] * 255), 2);
		cv::circle(new_disp, cv::Point(joints[color_ind][0], joints[color_ind][1]), 6, cv::Scalar(labels[color_ind][2] * 255, labels[color_ind][1] * 255 , labels[color_ind][0] * 255), 2);
	}

	//palm
	cv::circle(new_disp, cv::Point(joints[1][0], joints[1][1]), 6, cv::Scalar(0,0,255), 2);
	return new_disp;
}

void get_joints(hskl::Tracker& g_tracking, float** joints){

	//float joints[17][3] = {0};

	// Palm
	hskl::float3 v;
	for(int i=0; i<g_tracking.GetBoneCount();  i++)
	{

		if(!(i==2 || i==5 || i==8 || i==11)) continue;
		int parent = g_tracking.GetParentBone(i); if(parent == HSKL_BAD_INDEX) continue;
		auto v0 = g_tracking.GetPosition(i);
		auto v1 = g_tracking.GetPosition(parent);
		v = v + (v0+v1)*.5;

		joints[1][0] = v.x;
		joints[1][1] = v.y;
		joints[1][2] = v.z;
	}

	v = v*.25;
	// Joints
	int joint_mapping[] = {0, -1, -1, 14,15,-1, 11,12,-1, 8,9,-1, 5,6,-1, 2,3,-1};
	int tip_mapping[] = {16, 13, 10, 7, 4};

	int ind = 1;
	for(int i=0; i<g_tracking.GetBoneCount();  i++)
	{
		ind = joint_mapping[i];

		if(ind==-1) continue;

		//glColor3f(labels[ind][0],labels[ind][1],labels[ind][2]);
		
		int parent = g_tracking.GetParentBone(i); if(parent == HSKL_BAD_INDEX) continue;
		auto v0 = g_tracking.GetPosition(i);
		auto v1 = g_tracking.GetPosition(parent);
		auto v = (v0+v1)*.5;

		joints[ind][0] = v.x;
		joints[ind][1] = v.y;
		joints[ind][2] = v.z;
	}
	// to fingertips
	for(int finger=0; finger<5; ++finger)  
	{
		auto v0 = g_tracking.GetBoneTip(4+3*finger);
		auto v1 = g_tracking.GetPosition(4+3*finger);
		hskl::float3 v = (v0+v1)*.5;

		int ind = tip_mapping[finger];
		//glColor3f(labels[ind][0],labels[ind][1],labels[ind][2]);


		joints[ind][0] = v.x;
		joints[ind][1] = v.y;
		joints[ind][2] = v.z;
	}




	for(int i=1; i<17; ++i)
	{
		hskl::float4 v(joints[i][0], joints[i][1], joints[i][2], 1);

		v = hglToScreen(v);

		joints[i][0] = v.x/2.;
		joints[i][1] = v.y/2.;
		joints[i][2] = a[i-1]*v.z+b[i-1];


	}
}
struct triplet{
	int x;
	int y;
	float z;
	triplet(int x = 0, int y = 0, float z = 0):x(x),y(y),z(z){};
};

std::vector<triplet> get_joint_screen_locations(std::vector<std::string>& gt_line_tokens){
	using namespace std;

	int16_t max_joint_depth = 0;
	vector<triplet> joints_screen_coords;
	for (int i = 0; i < gt_line_tokens.size(); i += 3) {
		float x = atof(gt_line_tokens.at(i).c_str());
		float y = atof(gt_line_tokens.at(i + 1).c_str());
		float z = atof(gt_line_tokens.at(i + 2).c_str());

		triplet t(x, y, z);
		joints_screen_coords.push_back(t);
	}

	return joints_screen_coords;
}


std::vector<triplet> get_joint_triplets(float **joints){
	std::vector<triplet> results;
	//start from 1 as 0th joint represents forearm
	for(int i = 1; i < 17; i++){
		triplet t(joints[i][0], joints[i][1], joints[i][2]);
		results.push_back(t);
	}
	return results;
}

double euclidean_distance(triplet& a, triplet& b){
	double val = std::pow(a.x - b.x, 2.f) + std::pow(a.y - b.y, 2.f) +std::pow(a.z - b.z, 2.f);
	return std::sqrt(val);
}

std::string get_timestamp_from_training_file(std::string& filename){
        std::string training_prefix = "training/training_";
        std::string training_suffix = ".txt";
		
		int index = filename.find(training_prefix);
		if(index != std::string::npos){
			std::string timestamp = filename.substr(index + training_prefix.size(), filename.size());
			timestamp = timestamp.substr(0, timestamp.size() - training_suffix.size());
			return timestamp;	
		}

		else{
			std::cerr << " couldn't find the substring " << std::endl;
			std::cin.get();
			exit(-1);
		}
}


int get_image_number_from_file_id(std::string& file_id){
	using namespace std;
	//####.png
	string image_id = file_id.substr(file_id.size() - 8, file_id.size());
	string num = image_id.substr(0, image_id.size() - 4);
	int number = atoi(num.c_str());
	return number;
}

void duplicate_files(std::string& training_file, std::string& out_depth, std::string& out_rgb, std::string& out_ir, std::string& out_training, int number_of_duplications){
	using namespace std;
	using namespace boost::filesystem;


	
	std::ifstream infile(training_file.c_str());
	std::string line;
	std::string out_timestamp = get_timestamp_from_training_file(out_training);
	cout <<" out_timestamp is "<< out_timestamp << endl;
	std::string timestamp = get_timestamp_from_training_file(training_file);
	cout << "timestamp is "<< timestamp << endl;
	int number_of_images = 0;

	map<int, vector<string> > image_id_to_ids;

	while (std::getline(infile, line))
	{
		istringstream iss(line);

		//split line by space into tokens
		vector<string> tokens;
		copy(istream_iterator<string>(iss),istream_iterator<string>(),back_inserter<vector<string> >(tokens));
		string file_id = tokens.at(0);

		tokens.erase(tokens.begin() + 0);

		stringstream dfs;
		dfs << "D:/Intel_Real_Training/Depth/" << file_id; 

		stringstream ir_file;
		ir_file << "D:/Intel_Real_Training/IR/" << file_id;

		stringstream rgb_file;
		rgb_file << "D:/Intel_Real_Training/RGB/" << file_id;

		boost::filesystem::path depth_file = boost::filesystem::path(dfs.str().c_str());
		if (exists(depth_file)) {
			int image_number = get_image_number_from_file_id(file_id);
			image_id_to_ids[image_number] = tokens;
		}	
		number_of_images++;
	}

	cout << " counted " << number_of_images << " images  " << endl;

	stringstream dp;
	dp << "D:/Intel_Real_Training/Depth/" << timestamp << "/";
	string depth_prefix = dp.str();

	stringstream rp;
	rp << "D:/Intel_Real_Training/RGB/" << timestamp << "/";
	string rgb_prefix = rp.str();

	stringstream ip;
	ip << "D:/Intel_Real_Training/IR/" << timestamp << "/";
	string ir_prefix = ip.str();

	
	std::ofstream outfile(out_training.c_str());

	for(int i = 0; i < number_of_images; i++){

		stringstream filenamess;
		filenamess << "image_" ;

		if(i < 10){
			filenamess << "000";
		}
		else if(i < 100){
			filenamess << "00";
		}
		else if(i < 1000){
			filenamess << "0";
		}

		filenamess << i << ".png"; 
		stringstream rgb_ss;
		rgb_ss << rgb_prefix << filenamess.str();

		stringstream ir_ss;
		ir_ss << ir_prefix << filenamess.str();

		stringstream depth_ss;
		depth_ss << depth_prefix << filenamess.str();

		cout <<" filename is " << filenamess.str() << endl;

		boost::filesystem::path depth_file = boost::filesystem::path(depth_ss.str());
		boost::filesystem::path rgb_file = boost::filesystem::path(rgb_ss.str());
		boost::filesystem::path ir_file = boost::filesystem::path(ir_ss.str());
		
		string joint_locations;
		vector<string> joint_tokens = image_id_to_ids[i];
		joint_locations = boost::algorithm::join(joint_tokens, " ");
		cout << "got joint locations "<< endl;
		int starting_num = i*number_of_duplications;

		for(int j = starting_num; j < starting_num + number_of_duplications; j++){
			stringstream out_file_id;
			out_file_id << "image_";
			if(j < 10){
				out_file_id << "000";
			}
			else if(j < 100){
				out_file_id << "00";
			}
			else if(j < 1000){
				out_file_id << "0";
			}
			out_file_id << j << ".png";
			cout << "out file id is " << out_file_id.str() << endl;
			stringstream rgb_out_ss;
			rgb_out_ss << out_rgb << out_file_id.str();
			
			stringstream depth_out_ss;
			depth_out_ss << out_depth << out_file_id.str();

			stringstream ir_out_ss;
			ir_out_ss << out_ir << out_file_id.str();

			cout << "rgb int file is "<< rgb_file.string() << endl;
			cout << "rgb out file is "<< rgb_out_ss.str() << endl;

			boost::filesystem::path out_file_rgb = boost::filesystem::path(rgb_out_ss.str());
			boost::filesystem::path out_file_depth = boost::filesystem::path(depth_out_ss.str());
			boost::filesystem::path out_file_ir = boost::filesystem::path(ir_out_ss.str());

			boost::filesystem::copy_file(depth_file, out_file_depth);
			boost::filesystem::copy_file(rgb_file, out_file_rgb);
			boost::filesystem::copy_file(ir_file, out_file_ir);

			// add to training file

			stringstream out_line;
			out_line << out_timestamp << out_file_id << " " << joint_locations;
			outfile << out_line.str() << endl;
		}
		cout << "done for file " << i << endl;
	}
	outfile.flush();
	outfile.close();
}


int main(int argc, char * argv[])
{
	using namespace std;
	using namespace boost::filesystem;
	using namespace cv;



	/* values from sensor are 
	*  dimx = 320
	*  dimy = 240
	*  fovx = 1.23838
	*  fovy = 0.960016
	*/ 
	//INIT

	hg_tracking.dimx = 320;
	hg_tracking.dimy = 240;
	hg_tracking.fovx = 1.23838f;
	hg_tracking.fovy = 0.960016f;
	hg_tracking.color = new unsigned char[hg_tracking.dimx*hg_tracking.dimy*3];
	hg_tracking.depth = new unsigned short[hg_tracking.dimx*hg_tracking.dimy];
	hg_tracking.ir = new unsigned short[hg_tracking.dimx*hg_tracking.dimy];

	//g_tracking.SetModelType(HSKL_MODEL_LEFT_HAND );
	hg_tracking.tracker = hsklCreateTracker(HSKL_COORDS_X_RIGHT_Y_DOWN_Z_FWD, HSKL_API_VERSION);
	hsklSetSensorProperties(hg_tracking.tracker, HSKL_SENSOR_CREATIVE, hg_tracking.dimx, hg_tracking.dimy, hg_tracking.fovx, hg_tracking.fovy);
	hg_tracking.SetModelType(hskl_model::HSKL_MODEL_LEFT_HAND);
	//hg_tracking.SetHandMeasurements(0.0618, 0.1687);
	hg_tracking.SetHandMeasurements(0.0696, 0.1755);

	//For each file we must read in and simulate update
	string depth_prefix = "D:/Intel_Real_Training/Depth/201310231710/";
	string rgb_prefix = "D:/Intel_Real_Training/RGB/201310231710/";
	string ir_prefix = "D:/Intel_Real_Training/IR/201310231710/";
	string gt_file = "D:/Intel_Real_Training/training/training_201310231710.txt";

	
	//string out_depth = "D:/Intel_Real_Training/Depth/201310231710_2/";
	//string out_rgb = "D:/Intel_Real_Training/RGB/201310231710_2/";
	//string out_ir = "D:/Intel_Real_Training/IR/201310231710_2/";
	string out_training = "D:/Intel_Real_Training/training/training_201310231710_2.txt";
	string out_prefix = "201310231710/";


	string out_folder ="D:/Intel_Real_Training/sub_sample/201310231710/";



	directory_iterator end;
	for(directory_iterator it(out_folder); it != end; it++)
    {	
		boost::filesystem::remove(it->path());
	}


	std::ofstream outfile(out_training.c_str());

	cout << " gt file is "<< gt_file << endl;

	//load ground truth for joints
	std::map<int, std::vector<triplet> > file_number_to_joints;

	string postfix_to_remove = ".png";

	std::ifstream infile(gt_file.c_str());
	std::string line;
	while (std::getline(infile, line))
	{

		istringstream iss(line);

		//split line by space into tokens
		vector<string> tokens;
		copy(istream_iterator<string>(iss),istream_iterator<string>(),back_inserter<vector<string> >(tokens));

		string file_id = tokens.at(0);

		tokens.erase(tokens.begin() + 0);

		stringstream dfs;
		dfs << "D:/Intel_Real_Training/Depth/" << file_id; 

		boost::filesystem::path depth_file = boost::filesystem::path(dfs.str().c_str());
		if (exists(depth_file)) {
			vector<triplet> joints= get_joint_screen_locations(tokens);
			string file_number = file_id.substr(file_id.size() - postfix_to_remove.size() - 4, file_id.size() - postfix_to_remove.size());
			int file_index = atoi(file_number.c_str());

			file_number_to_joints[file_index] = joints;
		}	
	}
	
	/*
	Create subfiles -- every 3rd frame
	*/


	directory_iterator end_itr; //default construction provides an end reference
	int image_count = 0;
	for (directory_iterator itr(depth_prefix); itr != end_itr; itr++) {
		image_count ++;
	}
	cout << " read ground truth " << endl;
	cout << "there are " << image_count << "images " << endl;

	
	vector<int> numbers_to_check;
	numbers_to_check.push_back(1);
	//numbers_to_check.push_back(3);
	//numbers_to_check.push_back(5);
	//numbers_to_check.push_back(7);
	//numbers_to_check.push_back(10);
	//numbers_to_check.push_back(12);
	//numbers_to_check.push_back(15);



	//for(vector<int>::iterator num = numbers_to_check.begin(); num != numbers_to_check.end(); num++){
		//cout << "doing for every " << *num << " frames " << endl;

		//map<int, std::vector<triplet> > test_results;

		int num = 1;
		for(int i = 0; i < image_count ; i++){
			if(i % num == 0){
				stringstream filenamess;
				filenamess << "image_" ;

				if(i < 10){
					filenamess << "000";
				}
				else if(i < 100){
					filenamess << "00";
				}
				else if(i < 1000){
					filenamess << "0";
				}

				filenamess << i << ".png"; 
				stringstream rgb_ss;
				rgb_ss << rgb_prefix << filenamess.str();
				
				stringstream ir_ss;
				ir_ss << ir_prefix << filenamess.str();

				stringstream depth_ss;
				depth_ss << depth_prefix << filenamess.str();

				string depth_filepath = depth_ss.str();
				string rgb_filepath = rgb_ss.str();
				string ir_filepath = ir_ss.str();

				

				Mat depth = imread(depth_filepath, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
				Mat ir = imread(ir_filepath, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

				Mat display = Mat(ir.rows, ir.cols, CV_8UC3, cv::Scalar(0,0,0));
				for(int y = 0; y < ir.rows; y++){
					for(int x =0; x < ir.cols; x++){
						display.at<Vec3b>(y,x)[0] = (uchar)(ir.at<int16_t>(y,x) >> 2);
						display.at<Vec3b>(y,x)[1] = (uchar)(ir.at<int16_t>(y,x) >> 2);
						display.at<Vec3b>(y,x)[2] = (uchar)(ir.at<int16_t>(y,x) >> 2);
					}
				}

				//cv::flip(display, display, -1);
				for(int k = 0; k < 60; k++){
					hg_tracking.depth = reinterpret_cast<unsigned short*>(depth.data);
					const unsigned short * conf = reinterpret_cast<const unsigned short *>(ir.data);
					hsklTrackOneFrame(hg_tracking.tracker, hg_tracking.depth, conf); // Pass data to tracking library
				}


				cout << "rgb is " << rgb_filepath << endl << " ir is " << ir_filepath << endl << " depth is " << depth_filepath << endl;
				float **joints;
				joints = new float*[17];
				for(int k = 0; k < 17; k++){
					joints[k] = new float[3];
				}

				get_joints(hg_tracking, joints);
				Mat new_disp = display_joints(hg_tracking, joints, display);
				
				/*
				vector<triplet>  joint_triplets = get_joint_triplets(joints);
				test_results[i] = joint_triplets;
				cv::Mat v_depth = get_visualizable_depth_img(depth);
				imshow("depth", v_depth);
				imshow("wind", new_disp);*/

				stringstream out_image_name;
				out_image_name << out_folder << filenamess.str(); 
				imwrite(out_image_name.str(), new_disp);

				outfile << out_prefix << filenamess.str();
				
				for(int k = 1; k < 17; k++){
					outfile << " " << joints[k][0]  << " " << joints[k][1] << " " << joints[k][2];
					
				}
				outfile << endl;
				

				//waitKey(0);
				delete[](joints);
			}
		}
		outfile.flush();
		outfile.close();
/*
		cout <<" done images calculating joint errors " << endl;
		//calculate average error for each joint

		map<int, vector<double> > joint_to_error;

		for(map<int, vector<triplet> >::iterator res = test_results.begin(); res != test_results.end(); res++){
			int image_id = res->first;

			vector<triplet>& test = res->second;
			vector<triplet>& gt = file_number_to_joints[image_id];

			for(int i = 0; i < 16; i++){
				double euc_dist = euclidean_distance(test.at(i), gt.at(i));
				joint_to_error[i].push_back(euc_dist);
			}	
		}

		map<int, double> avg_error;
		cout << "average errors (for every " << *num << " frame:" << endl;
		for(int i = 0; i < 16; i++){
			double sum = 0;
			for(vector<double>::iterator it = joint_to_error[i].begin(); it != joint_to_error[i].end(); it++){
				sum += *it;
			}
			avg_error[i] = sum / ((double)joint_to_error[i].size());
			cout << "for joint " << i <<": " << avg_error[i] << endl;
		}*/
	//}
	
	//cin.get();
}
