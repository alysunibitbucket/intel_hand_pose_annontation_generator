// The aim of this sample is to show a well-documented example of how to use our tracking with minimal code
#include "hsklu.h" // Tracking library API + 3D depth sensor support
#include <gl/freeglut.h> // For rendering output (OpenGL/FreeGLUT)
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <direct.h>
#include <string>

using namespace std;
hskl::Tracker	g_tracking;								// Tracking library context
float			g_handWidth  = 0.08f;					// Width of hand, assume 8 cm to start
float			g_handLength = 0.19f;					// Length of hand, assume 19 cm to start
const char *	g_profileString = "left hand";			// Description of which hand is being tracked

int				g_winWidth, g_winHeight;				// Width and height of the rendering window
const float		g_nearZ = .01f, g_farZ = 2.f;			// Near and far clip distances for rendering
int				g_time0, g_time1, g_numFrames, g_fps;	// Timing control variables
bool			g_layers[5] = {false,true,false,true,true};	// Rendering layers

bool			g_recording = false;
int				g_counter = 0;

cv::RNG			g_rng(12345);

std::ofstream	g_file;

time_t t = time(0);
struct tm* g_timestamp = gmtime(&t);

const float labels[17][3] = { {1, 1, 1}, // Forearm
	{1, 0, 0}, // Palm
    {0, .5, 0}, // Thumb1
    {0, 1, 0}, // Thumb2
    {.5, 1, .5}, // Thumb3
    {.5, 0, .5}, // Index1
    {1, 0, 1}, // Index2
    {1, .5, 1}, // Index3
    {.5, .5, 0}, // Middle1
    {1, 1, 0}, // Middle2
    {1, 1, .5}, // Middle3
    {0, .5, .5}, // Ring1
    {0, 1, 1}, // Ring2
    {.5, 1, 1}, // Ring3
    {0, 0, .5}, // Pinky1
    {0, 0, 1}, // Pinky2
    {.5, .5, 1} // Pinky3
};

const float a[16] = {252.686450, 1006.670652, 1001.850891, 1052.659947, 1009.336381, 997.363478, 994.278576, 1011.075164, 1001.215584, 998.850565, 1003.479420, 1002.230528, 1013.421658, 1000.341925, 1011.938343, 1012.794318};
const float b[16] = { -20.2262,  -24.7399,   -7.0172,  -23.5472,  -14.8356,  -13.1651,   -9.0130,  -15.0631, -14.0838,  -11.9694,  -12.2970,  -12.9018,  -15.9086,   -8.4984,  -11.3813,  -12.2925};

hskl::float4 glToScreen(const hskl::float4 v) {
    
    // Get the matrices and viewport
    double modelView[16];
	//hskl::float4x4 modelView(hskl::float4(1,0,0,0), hskl::float4(0,-1,0,0), hskl::float4(0,0,-1,0), hskl::float4(0,0,0,1));
    double projection[16];
	//hskl::float4x4 projection = g_tracking.GetSensorPerspectiveMatrix(150,1000);
    double viewport[4];
	double depthRange[2];// = {200, 100000};//{150, 500};

    glGetDoublev(GL_MODELVIEW_MATRIX, modelView);
    glGetDoublev(GL_PROJECTION_MATRIX, projection);
    glGetDoublev(GL_VIEWPORT, viewport);
    glGetDoublev(GL_DEPTH_RANGE, depthRange);
	/*

	std::cout << "model view is :" << std::endl;
	std::cout << "[";
	for(int i = 0; i < 16; i++){
		std::cout << modelView[i] << ", ";
	}
	std::cout << "]"<< std::endl;
	

	std::cout <<" projection is : "<< std::endl;
	std::cout << "[";
	for(int i =0 ; i < 16; i++){
		std::cout << projection[i] << ", " ;
	}
	std::cout << "]"<<std::endl;

	std::cout << " viewport is " << std::endl;
	std::cout << "[";
	for(int i =0 ; i < 4; i++){
		std::cout << viewport[i] << ", ";
	}
	std::cout << "]" <<std::endl;

	std::cout << "depthRange is "<< std::endl;
	std::cout << "[";
	for(int i =0; i < 2; i++){
		std::cout << depthRange[i] << ", ";
	}
	std::cout << "]"<<std::endl;
	std::cin.get(); */

    // Compose the matrices into a single row-major transformation
    float T[4][4];
	//hskl::float4x4 T;
    int r, c, i;
    for (r = 0; r < 4; ++r) {
        for (c = 0; c < 4; ++c) {
            T[r][c] = 0;
            for (i = 0; i < 4; ++i) {
                // OpenGL matrices are column major
                T[r][c] += projection[r + i * 4] * modelView[i + c * 4];
            }
        }
    }

	hskl::float4 T1[4];
	for (r=0; r<4; ++r) {
		T1[r] = hskl::float4(T[r][0], T[r][1], T[r][2],T[r][3]);
	}

    // Transform the vertex
	hskl::float4 result;
	result.x = hskl::dot(T1[0], v);
	result.y = hskl::dot(T1[1], v);
	result.z = hskl::dot(T1[2], v);
	result.w = hskl::dot(T1[3], v);

	//if(result.w==0) std::cout << T1[3][0] << "," << T1[3][1] << "," << T1[3][2] << "," << T1[3][3] << " " << v[0] << " " << v[1] << " " << v[2] << " " << v[3]  << std::endl;

    // Homogeneous divide
    const double rhw = 1 / result.w;


	double depth = (result.z * rhw) * (depthRange[1] - depthRange[0]) + depthRange[0];


	if(depth < 1.0){
		depth = -1 * ((g_farZ * g_nearZ)/(depth*(g_farZ - g_nearZ) - g_farZ));

		//depth = 2*z_far*z_near/(z_far + z_near - (depth*2 - 1)*(z_far - z_near) );

	}
	else
	{
		depth = pow(2.,15)-1;
	}

	
    return hskl::float4(
        (1 + result.x * rhw) * viewport[2] / 2 + viewport[0],
        (1 - result.y * rhw) * viewport[3] / 2 + viewport[1],
		v.z, // Coefficients calculated from linear regression
        //depth,
		//(result.z * rhw) * (depthRange[1] - depthRange[0]) + depthRange[0]
		//(result.z * rhw) * (g_farZ - g_nearZ) + g_nearZ,
		//(result.z * rhw - g_nearZ) * (depthRange[1] - depthRange[0]) / (g_farZ - g_nearZ) + depthRange[0],
		//(result.z * rhw),
        rhw);

} 

void UpdateTracking()
{
	g_tracking.Update();
	if(++g_numFrames == 4) { g_time1 = glutGet(GLUT_ELAPSED_TIME); g_fps = 1000 * g_numFrames / (g_time1-g_time0); g_time0 = g_time1; g_numFrames = 0; }
	glutPostRedisplay();
}

void OnKeyboard(unsigned char ch, int x, int y)
{
	// If user hit escape, clean up properly and quit
	if(ch == 27)
	{
		exit(0);
	}

	// Otherwise let user resize the hand based on two scaling factors
	switch(ch)
	{
	case 'w': g_handLength *= 1.02f; break;
	case 'a': g_handWidth  /= 1.02f; break;
	case 's': g_handLength /= 1.02f; break;
	case 'd': g_handWidth  *= 1.02f; break;
	case 'r': g_tracking.SetModelType(HSKL_MODEL_RIGHT_HAND); g_profileString = "right hand"; break;
	case 'l': g_tracking.SetModelType(HSKL_MODEL_LEFT_HAND ); g_profileString = "left hand";  break;
	case 't': g_tracking.SetModelType(HSKL_MODEL_TWO_HAND  ); g_profileString = "both hands"; break;
	case '1': case '2': case '3': case '4': g_layers[ch-'1'] = !g_layers[ch-'1']; break;
	case 'p': 
		char date[13];
		strftime(date, sizeof(date), "%Y%m%d%H%M", g_timestamp);

		char buffer[300];
		sprintf(buffer, "D:/Intel_Real_Training/RGB/%s/", date);
		mkdir(buffer);

		sprintf(buffer, "D:/Intel_Real_Training/Depth/%s/", date);
		mkdir(buffer);

		sprintf(buffer, "D:/Intel_Real_Training/IR/%s/", date);
		mkdir(buffer);
		
		sprintf(buffer,"D:/Intel_Real_Training/training/");
		mkdir(buffer);

		sprintf(buffer,"D:/Intel_Real_Training/training/%s.txt", date);

		g_recording = !g_recording; 
		
		if(g_recording) 
			g_file.open (buffer, std::ios::app);
		else
			g_file.close();
		
		break;
	}
	g_tracking.SetHandMeasurements(g_handWidth, g_handLength);
}

// Utility function to write a printf style string on the screen
void DrawString(int x, int y, const char * format, ...)
{
	glRasterPos2i(x,y);
	va_list args; char buffer[1024];
	va_start(args, format);
	int n = vsnprintf(buffer, sizeof(buffer), format, args);
	for(int i=0; i<n; ++i) glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, buffer[i]);
	va_end(args);
}

void OnDisplay()
{
	if(g_tracking.GetColorImageBuffer() == 0) return;
	// Render
	glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

	float joints[17][3] = {0};

	// Draw color buffer from camera
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glPixelZoom((float)g_winWidth/g_tracking.GetColorImageWidth(), -(float)g_winHeight/g_tracking.GetColorImageHeight()); 
	glRasterPos2f(-1, 1);
	if(g_layers[4])
	{
		glDrawPixels(g_tracking.GetColorImageWidth(), g_tracking.GetColorImageHeight(), GL_RGB, GL_UNSIGNED_BYTE, g_tracking.GetColorImageBuffer());
	}
	if(g_layers[3])
	{
		auto dimx = g_tracking.GetDepthImageWidth(), dimy = g_tracking.GetDepthImageHeight();

		glMatrixMode(GL_PROJECTION); glPushMatrix(); glOrtho(0, dimx, dimy, 0, -1, 1); glBegin(GL_POINTS);

		for(int y=0; y<dimy; ++y) for(int x=0; x<dimx; ++x)
		{
			int segment = g_tracking.GetSegmentationMask()[y*dimx+x];
			if(segment > 0) { glColor3fv(segment == 1 ? hskl::float3(0,1,1) : hskl::float3(1,0,1)); glVertex2i(x,y); }
		}
		glEnd(); glPopMatrix();
	}

	glPopAttrib();
	glClear(GL_DEPTH_BUFFER_BIT); // on Intel Graphics we noticed drawpixels seemed to affect the depth buffer

	// Set up OpenGL state
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glEnable(GL_BLEND);	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_NORMALIZE);	glEnable(GL_COLOR_MATERIAL); glEnable(GL_CULL_FACE);

	// Use a left-handed perspective projection matrix whose horizontal and vertical field of view mimic the sensor device
	glMatrixMode(GL_PROJECTION); glPushMatrix(); glMultMatrixf(g_tracking.GetSensorPerspectiveMatrix(g_nearZ,g_farZ)); 
	float light_position[] = {0,0,0,1}; glEnable(GL_LIGHT0); glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMatrixMode(GL_MODELVIEW); glPushMatrix(); gluLookAt(0,0,0, 0,0,1, 0,-1,0);


	// Draw meshes and basis vectors for each bone
	//int tips[] = {0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1, 0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1};
	for(int i=0; i<g_tracking.GetBoneCount();  i++)
	{
		// Set up transformation for bone
		glPushMatrix(); glMultMatrixf(g_tracking.GetBoneModelMatrix(i));
		glEnable(GL_DEPTH_TEST); glEnable(GL_LIGHTING); 
		if(g_layers[0]) // Draw bone geometry used inside tracking library (in a real application you would use a skinned mesh of some sort)
		{
			glBegin(GL_TRIANGLES);
			if(g_tracking.GetTrackingError(i) < 0.5f) glColor4f(0.3f,0.3f,1,0.75f);
			else glColor4f(0.7f,0.3f,1,0.75f);
			auto verts = g_tracking.GetVertices(i);
			auto tris = g_tracking.GetTriangles(i);

			for(int j=0; j<g_tracking.GetTriangleCount(i); ++j)
			{
				// Compute a normal from the vertices of this bone. This yields a "flat shaded" look on the mesh.
				auto v0 = verts[tris[j].x], v1 = verts[tris[j].y], v2 = verts[tris[j].z];
				glNormal3fv(cross(v1-v0,v2-v0)); glVertex3fv(v0); glVertex3fv(v1); glVertex3fv(v2);
			}
			glEnd();
		}

		glDisable(GL_DEPTH_TEST); glDisable(GL_LIGHTING); 
		if(g_layers[2]) // Draw 1 cm long colored basis vectors for bone frame of reference
		{
			glBegin(GL_LINES);
			glColor3f(1,0,0); glVertex3f(0, 0, 0); glVertex3f(0.01f, 0, 0);
			glColor3f(0,1,0); glVertex3f(0, 0, 0); glVertex3f(0, 0.01f, 0);
			glColor3f(0,0.5f,1); glVertex3f(0, 0, 0); glVertex3f(0, 0, 0.01f);
			glEnd();
		}

		glPopMatrix();
	}

	if(g_layers[1]) // Draw "skeleton" from connections between bones and finger tip locations
	{
		// the position (local origin) of each bone is where it connects (joined) to its "parent" bone
		glBegin(GL_LINES); 
		for(int i=0; i<g_tracking.GetBoneCount();  i++)
		{
			int parent = g_tracking.GetParentBone(i); if(parent == HSKL_BAD_INDEX) continue;
			glVertex3fv(g_tracking.GetPosition(parent));  glVertex3fv(g_tracking.GetPosition(i));
		}
		glEnd();


		GLUquadricObj* quadric = gluNewQuadric();
		gluQuadricDrawStyle(quadric, GLU_FILL );

		
		// Palm
		std::cout << " doing palm in live mode " << std::endl;
		hskl::float3 v;
		for(int i=0; i<g_tracking.GetBoneCount();  i++)
		{
			glColor3f(1,0,0);

			if(!(i==2 || i==5 || i==8 || i==11)) continue;
			int parent = g_tracking.GetParentBone(i); if(parent == HSKL_BAD_INDEX) continue;
			auto v0 = g_tracking.GetPosition(i);
			auto v1 = g_tracking.GetPosition(parent);
			v = v + (v0+v1)*.5;

			joints[1][0] = v.x;
			joints[1][1] = v.y;
			joints[1][2] = v.z;
		}

		std::cout << "the palm position is [" << joints[1][0] << ", "<< joints[1][1] << ", "<< joints[1][2] << std::endl;
		v = v*.25;
		std::cout << " after the palm position is [" << joints[1][0] << ", "<< joints[1][1] << ", "<< joints[1][2] << std::endl;
		glPushMatrix();
		glEnable(GL_LIGHTING); 
			
		glTranslatef( v.x,v.y,v.z );
		gluSphere( quadric , .01 , 10 , 10 );
		glPopMatrix();

		// Joints
		int joint_mapping[] = {0, -1, -1, 14,15,-1, 11,12,-1, 8,9,-1, 5,6,-1, 2,3,-1};
		int tip_mapping[] = {16, 13, 10, 7, 4};

		int ind = 1;
		for(int i=0; i<g_tracking.GetBoneCount();  i++)
		{
			ind = joint_mapping[i];

			if(ind==-1) continue;

			glColor3f(labels[ind][0],labels[ind][1],labels[ind][2]);

			int parent = g_tracking.GetParentBone(i); if(parent == HSKL_BAD_INDEX) continue;
			auto v0 = g_tracking.GetPosition(i);
			auto v1 = g_tracking.GetPosition(parent);
			auto v = (v0+v1)*.5;
			glPushMatrix();
			glEnable(GL_LIGHTING); 
			
			glTranslatef( v.x,v.y,v.z );
			gluSphere( quadric , .006 , 10 , 10 );
			glPopMatrix();

			joints[ind][0] = v.x;
			joints[ind][1] = v.y;
			joints[ind][2] = v.z;
		}

		for(int finger=0; finger<5; ++finger)  // to fingertips
		{
			auto v0 = g_tracking.GetBoneTip(4+3*finger);
			auto v1 = g_tracking.GetPosition(4+3*finger);
			hskl::float3 v = (v0+v1)*.5;

			int ind = tip_mapping[finger];
			glColor3f(labels[ind][0],labels[ind][1],labels[ind][2]);

			glPushMatrix();
			glEnable(GL_LIGHTING); 
			glTranslatef( v.x,v.y,v.z );
			gluSphere( quadric , .005 , 10 , 10 );
			glPopMatrix();

			joints[ind][0] = v.x;
			joints[ind][1] = v.y;
			joints[ind][2] = v.z;
		}

		for(int i=1; i<17; ++i)
		{
			hskl::float4 v(joints[i][0], joints[i][1], joints[i][2], 1);
			std::cout << "joints before are (" << joints[i][0] << ", " << joints[i][1] << ", " << joints[i][2] << std::endl;
			v = glToScreen(v);

			joints[i][0] = v.x/2.;
			joints[i][1] = v.y/2.;
			joints[i][2] = a[i-1]*v.z+b[i-1];

			std::cout << "joints after are (" << joints[i][0] << ", " << joints[i][1] << ", " << joints[i][2] << std::endl;
			//joints[i][2] = v.z;
			
			//if(i==1) std::cout << joints[i][0] << ',' << joints[i][1] << ',' << joints[i][2] << std::endl;
			//',' << v.w << ' ' << g_winWidth << ',' << g_winHeight <<  std::endl;
		}

		gluDeleteQuadric(quadric); 

		// there isn't a "joint" at the tips/ends of each of the last bone of each finger, so we 
		// use the GetBoneTip method to walk to the upper (local-z) extreme of the bone
		glBegin(GL_LINES); 		
		for(int finger=0; finger<5; ++finger)  // to fingertips
		{
			auto p = g_tracking.GetBoneTip(4+3*finger);
			glColor3f(1,1,1);
			glVertex3fv(g_tracking.GetPosition(4+3*finger)); 
			glVertex3fv(p);
			glColor3f(1,1,0);
			glVertex3f(p.x-0.002f,p.y,p.z); glVertex3f(p.x+0.002f,p.y,p.z);
			glVertex3f(p.x,p.y-0.002f,p.z); glVertex3f(p.x,p.y+0.002f,p.z);
		}
		glEnd();
	}

	glPopMatrix(); glMatrixMode(GL_PROJECTION); glPopMatrix(); glPopAttrib();

	// Draw overlay text
	glPushMatrix(); glOrtho(0,g_winWidth,g_winHeight,0,-1,1);
	DrawString(10,24, "Tracking %s at %d FPS with error of %0.4f (Press r/l/t to change) [%d bit]", g_profileString, g_fps, g_tracking.GetTrackingError(0), sizeof(void *)*8);
	DrawString(10,40, "Assuming hand width of %0.2f cm and hand length of %0.2f cm (Press w/a/s/d to adjust)", g_handWidth*100, g_handLength*100);
	DrawString(10,56, "Displayed layers: 1-mesh %s, 2-skeleton %s, 3-bases %s, 4-segmentation %s (Press keys 1/2/3/4 to toggle)", g_layers[0]?"on":"off", g_layers[1]?"on":"off", g_layers[2]?"on":"off", g_layers[3]?"on":"off");

	if(g_recording)
	{
		//std::cout << "pausing for data capture " << std::endl;
		//std::cin.get();

		DrawString( 100, 100, "Recording.... %d images", g_counter );

		//if(g_rng.uniform(1, 10)<3)
		if(g_rng.uniform(1, 10)<11)
		{
			char buffer[300];
		
			const unsigned short* depth = g_tracking.GetDepthImageBuffer();

			//std::cout << g_tracking.GetColorImageHeight() << ',' << g_tracking.GetColorImageWidth() << std::endl;

			char date[13];
			strftime(date, sizeof(date), "%Y%m%d%H%M", g_timestamp);

			sprintf(buffer, "%s/image_%04d.png", date, g_counter);
			g_file << buffer << ' ';

			cv::Mat depthMat(g_tracking.GetDepthImageHeight(), g_tracking.GetDepthImageWidth(), CV_16UC1, (void*)g_tracking.GetDepthImageBuffer());
			sprintf(buffer, "D:/Intel_Real_Training/Depth/%s/image_%04d.png", date, g_counter);		
			imwrite(buffer, depthMat);

			cv::Mat irMat(g_tracking.GetDepthImageHeight(), g_tracking.GetDepthImageWidth(), CV_16UC1, (void*)g_tracking.GetIRImageBuffer());
			sprintf(buffer, "D:/Intel_Real_Training/IR/%s/image_%04d.png", date, g_counter);
			imwrite(buffer, irMat);

			GLbyte* mPixels = new GLbyte[g_winWidth*g_winHeight*3];
			glReadPixels(0,0,g_winWidth,g_winHeight,GL_RGB,GL_UNSIGNED_BYTE,mPixels);

			cv::Mat rgbMat(g_winHeight,g_winWidth, CV_8UC3, mPixels);
			cvtColor(rgbMat, rgbMat, CV_RGB2BGR); 

			sprintf(buffer, "D:/Intel_Real_Training/RGB/%s/image_%04d.png", date, g_counter);
			imwrite(buffer, rgbMat);

			for(int i=1; i<17; ++i)
			{
				for(int j=0; j<3; ++j)
				{
					g_file << joints[i][j] << ' ';
				}
			}

			g_file << std::endl;

			g_counter++;
		}
	}

	glPopMatrix();
	glutSwapBuffers();
}


void OnDisplayLabels()
{
	if(g_tracking.GetColorImageBuffer() == 0) return;
	// Render
	glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

	float joints[17][3] = {0};

	// Set up OpenGL state
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glEnable(GL_BLEND);	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_NORMALIZE);	glEnable(GL_COLOR_MATERIAL); glEnable(GL_CULL_FACE);

	// Use a left-handed perspective projection matrix whose horizontal and vertical field of view mimic the sensor device
	glMatrixMode(GL_PROJECTION); glPushMatrix(); glMultMatrixf(g_tracking.GetSensorPerspectiveMatrix(g_nearZ,g_farZ)); 
	float light_position[] = {0,0,0,1}; glEnable(GL_LIGHT0); glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMatrixMode(GL_MODELVIEW); glPushMatrix(); gluLookAt(0,0,0, 0,0,1, 0,-1,0);


	// Draw meshes and basis vectors for each bone
	//int tips[] = {0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1, 0,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1};
	for(int i=1; i<g_tracking.GetBoneCount();  i++)
	{
		// Set up transformation for bone
		glPushMatrix(); glMultMatrixf(g_tracking.GetBoneModelMatrix(i));
		//glEnable(GL_DEPTH_TEST); glEnable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST); glDisable(GL_LIGHTING);
		if(g_layers[0]) // Draw bone geometry used inside tracking library (in a real application you would use a skinned mesh of some sort)
		{
			glBegin(GL_TRIANGLES);
			glColor3f(labels[i][0],labels[i][1],labels[i][2]);
			auto verts = g_tracking.GetVertices(i);
			auto tris = g_tracking.GetTriangles(i);

			for(int j=0; j<g_tracking.GetTriangleCount(i); ++j)
			{
				// Compute a normal from the vertices of this bone. This yields a "flat shaded" look on the mesh.
				auto v0 = verts[tris[j].x], v1 = verts[tris[j].y], v2 = verts[tris[j].z];
				glNormal3fv(cross(v1-v0,v2-v0)); glVertex3fv(v0); glVertex3fv(v1); glVertex3fv(v2);
			}
			glEnd();
		}

		glPopMatrix();
	}

	if(g_recording)
	{
		DrawString( 100, 100, "Recording.... %d images", g_counter );

//		if(g_rng.uniform(1, 10)<3)
		if(g_rng.uniform(1, 10)<11)
		{
			char buffer[300];
		
			const unsigned short* depth = g_tracking.GetDepthImageBuffer();

			//std::cout << g_tracking.GetColorImageHeight() << ',' << g_tracking.GetColorImageWidth() << std::endl;

			char date[13];
			strftime(date, sizeof(date), "%Y%m%d%H%M", g_timestamp);

			sprintf(buffer, "%s/image_%04d.png", date, g_counter);
			g_file << buffer << ' ';

			cv::Mat depthMat(g_tracking.GetDepthImageHeight(), g_tracking.GetDepthImageWidth(), CV_16UC1, (void*)g_tracking.GetDepthImageBuffer());
			sprintf(buffer, "D:/Intel_Real_Training/Depth/%s/image_%04d.png", date, g_counter);
			imwrite(buffer, depthMat);

			cv::Mat irMat(g_tracking.GetDepthImageHeight(), g_tracking.GetDepthImageWidth(), CV_16UC1, (void*)g_tracking.GetIRImageBuffer());
			sprintf(buffer, "D:/Intel_Real_Training/IR/%s/image_%04d.png", date, g_counter);
			imwrite(buffer, irMat);

			GLbyte* mPixels = new GLbyte[g_winWidth*g_winHeight*3];
			glReadPixels(0,0,g_winWidth,g_winHeight,GL_RGB,GL_UNSIGNED_BYTE,mPixels);

			cv::Mat rgbMat(g_winHeight,g_winWidth, CV_8UC3, mPixels);
			cvtColor(rgbMat, rgbMat, CV_RGB2BGR); 
			sprintf(buffer, "D:/Intel_Real_Training/RGB/%s/image_%04d.png", date, g_counter);
			imwrite(buffer, rgbMat);

			for(int i=1; i<17; ++i)
			{
				for(int j=0; j<3; ++j)
				{
					g_file << joints[i][j] << ' ';
				}
			}

			g_file << std::endl;

			g_counter++;
		}
	}

	glPopMatrix();
	glutSwapBuffers();
}



int _main(int argc, char * argv[])
{
	if(!g_tracking.Init()) { fprintf(stderr, "Unable to initialize tracking\n"); return -1; }

	// Initialise GLUT and start running
	g_winWidth = g_tracking.GetColorImageWidth();
	g_winHeight = g_tracking.GetColorImageHeight();
	g_tracking.SetModelType(HSKL_MODEL_LEFT_HAND );
	
	if(g_winWidth < 500) { g_winWidth *= 2; g_winHeight *= 2; }

	glutInit(&argc,argv);

	glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
	glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - g_winWidth)/2, (glutGet(GLUT_SCREEN_HEIGHT) - g_winHeight)/2);
	glutInitWindowSize(g_winWidth, g_winHeight);

	int main_w = glutCreateWindow("Hand Pose Recognizer");
	glutKeyboardFunc(OnKeyboard);
	glutDisplayFunc(OnDisplay);
	glutIdleFunc(UpdateTracking);



	//glutCreateSubWindow(main_w, 10, 10, 200, 200);
	//glutDisplayFunc(OnDisplay);
	//int num = glutGet(GLUT_WINDOW_PARENT);
	//if (main_w != num) {
	//	printf("FAIL: glutGet returned bad parent: %d\n", num);
	//	exit(1);
	//}

	//glutCreateSubWindow(main_w, 210, 210, 200, 200);
	//glutDisplayFunc(OnDisplayLabels);
	//num = glutGet(GLUT_WINDOW_PARENT);
	//if (main_w != num) {
	//	printf("FAIL: glutGet returned bad parent: %d\n", num);
	//	exit(1);
	//}

	

	glutMainLoop();
	return 0;
}