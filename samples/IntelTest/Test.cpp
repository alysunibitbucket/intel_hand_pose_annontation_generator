#include "hsklu.h" // Tracking library API + 3D depth sensor support
#include <gl/freeglut.h> // For rendering output (OpenGL/FreeGLUT)
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <math.h>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <direct.h>
#include <string>


hskl::Tracker	g_tracking;								// Tracking library context
float			g_handWidth  = 0.08f;					// Width of hand, assume 8 cm to start
float			g_handLength = 0.19f;					// Length of hand, assume 19 cm to start
const char *	g_profileString = "left hand";			// Descripti: unexpected end of file while looking for precompiled header. Did you forget to add '#include "StdAfx.h"' to your sourceon of which hand is being tracked

int				g_winWidth, g_winHeight;				// Width and height of the rendering window
const float		g_nearZ = .01f, g_farZ = 2.f;			// Near and far clip distances for rendering
int				g_time0, g_time1, g_numFrames, g_fps;	// Timing control variables
bool			g_layers[5] = {false,true,false,true,true};	// Rendering layers

bool			g_recording = false;
int				g_counter = 0;


int main(int argc, char * argv[])
{

	return 0;
}
